package wiederholung.oop;

public class Verwaltung
{
	Mitarbeiter[] mitarbeiter = new Mitarbeiter[50];

	public static void main(String[] args)
	{
		Verwaltung verwaltung = new Verwaltung();
		verwaltung.sucheMitarbeiter(1234);
		verwaltung.sucheMitarbeiter("Mustermann");
	}

	public Verwaltung()
	{
		mitarbeiter[0] = new Mitarbeiter("Helga", "Boyd", 15904821, "Spie�", "04.08.1991", 2936.66);
		mitarbeiter[1] = new Mitarbeiter("Lupe", "Richardson", 77006797, "S1", "29.10.1975", 5337.41);
		mitarbeiter[2] = new Mitarbeiter("Santos", "Stout", 27423630, "S1", "26.05.2009", 4874.47);
		mitarbeiter[3] = new Mitarbeiter("Joseph", "Fernandez", 97925485, "S2", "01.03.1975", 3568.69);
		mitarbeiter[4] = new Mitarbeiter("Kelsey", "Blake", 27191661, "S2", "22.02.1998", 3965.69);
		mitarbeiter[5] = new Mitarbeiter("Kieth", "Atkins", 28795318, "S3", "18.08.2012", 5799.69);
		mitarbeiter[6] = new Mitarbeiter("Jeffery", "Lozano", 42360534, "S4", "05.02.2004", 4723.57);
		mitarbeiter[7] = new Mitarbeiter("Betsy", "Mcdaniel", 12404339, "S6", "15.06.1994", 2064.60);
		mitarbeiter[8] = new Mitarbeiter("Myles", "Hood", 60276982, "S6", "20.12.1989", 4721.62);
		mitarbeiter[9] = new Mitarbeiter("Timmy", "Hansen", 33043068, "S6", "06.01.2009", 6850.70);
		mitarbeiter[10] = new Mitarbeiter("Freida", "Brandt", 62683917, "Entwickler", "12.12.2004", 5914.50);
		mitarbeiter[11] = new Mitarbeiter("Garret", "Huynh", 10476677, "stv. Chef", "21.05.1994", 6907.22);
		mitarbeiter[12] = new Mitarbeiter("Elisha", "Houston", 30421902, "GeZi", "28.05.1973", 2841.33);
		mitarbeiter[13] = new Mitarbeiter("Lynwood", "Holden", 71075728, "Entwickler", "27.11.1975", 4778.45);
		mitarbeiter[14] = new Mitarbeiter("Palmer", "Suarez", 55466043, "Entwickler", "20.08.2004", 5441.98);
		mitarbeiter[15] = new Mitarbeiter("Kelli", "Smith", 65210494, "Praktikant", "29.10.1982", 2004.74);
		mitarbeiter[16] = new Mitarbeiter("Maria", "Dawson", 12208145, "Chef", "21.02.1996", 6953.57);
		mitarbeiter[17] = new Mitarbeiter("Kay", "Hobbs", 80055556, "GeZi", "13.05.1976", 2170.01);
		mitarbeiter[18] = new Mitarbeiter("Frankie", "Harrison", 22676899, "Entwickler", "19.03.1971", 2254.76);
		mitarbeiter[19] = new Mitarbeiter("Mayra", "Jennings", 53467130, "Entwickler", "28.06.1982", 4430.03);
		mitarbeiter[20] = new Mitarbeiter("Gilda", "Espinoza", 43069442, "Entwickler", "13.07.1992", 4901.34);
		mitarbeiter[21] = new Mitarbeiter("Millicent", "Hines", 25852388, "Entwickler", "09.10.1983", 6565.77);
		mitarbeiter[22] = new Mitarbeiter("Anastasia", "Banks", 45090374, "Entwickler", "19.07.1980", 6403.79);
		mitarbeiter[23] = new Mitarbeiter("Judy", "Bird", 91537268, "Entwickler", "18.06.2002", 6145.28);
		mitarbeiter[24] = new Mitarbeiter("Emil", "Tran", 46722145, "Entwickler", "05.02.2007", 3589.21);
		mitarbeiter[25] = new Mitarbeiter("Patrice", "Chung", 14887241, "QS", "20.07.1999", 6112.01);
		mitarbeiter[26] = new Mitarbeiter("Brad", "Warren", 17256566, "QS", "24.04.2005", 5991.87);
		mitarbeiter[27] = new Mitarbeiter("Tory", "Cohen", 66567675, "QS", "30.07.1982", 2127.49);
		mitarbeiter[28] = new Mitarbeiter("Leslie", "Arellano", 57177900, "QS", "10.01.1982", 6722.31);
		mitarbeiter[29] = new Mitarbeiter("Gwen", "Price", 40464544, "QS", "21.08.1971", 3736.96);
		mitarbeiter[30] = new Mitarbeiter("Jacques", "Watts", 84582843, "QS", "05.03.2009", 3973.28);
		mitarbeiter[31] = new Mitarbeiter("Scottie", "Bautista", 65511741, "QS", "08.05.1992", 5037.96);
		mitarbeiter[32] = new Mitarbeiter("Bob", "Casey", 46512219, "QS", "22.08.1993", 6371.42);
		mitarbeiter[33] = new Mitarbeiter("Rueben", "Fowler", 98309112, "Azubi", "07.06.1984", 3002.18);
		mitarbeiter[34] = new Mitarbeiter("Monty", "Shah", 28289348, "Stubenvergabe", "12.09.1982", 3868.38);
		mitarbeiter[35] = new Mitarbeiter("Jaime", "Crawford", 65316790, "Koch", "13.09.2008", 4795.53);
		mitarbeiter[36] = new Mitarbeiter("Andrew", "Wyatt", 18123412, "Koch", "05.05.1999", 5398.54);
		mitarbeiter[37] = new Mitarbeiter("Cortez", "English", 34553933, "Koch", "20.05.2000", 3072.86);
		mitarbeiter[38] = new Mitarbeiter("Jc", "Zuniga", 91981273, "Essensausgabe", "02.03.1980", 4591.53);
		mitarbeiter[39] = new Mitarbeiter("Lorrie", "West", 99468133, "Bereichsleiter", "22.04.1974", 6418.04);
		mitarbeiter[40] = new Mitarbeiter("Josefa", "Durham", 86741406, "Entwickler", "27.04.2008", 4599.78);
		mitarbeiter[41] = new Mitarbeiter("Connie", "Oneill", 10013061, "Entwickler", "22.04.1986", 5667.37);
		mitarbeiter[42] = new Mitarbeiter("Rex", "Dalton", 31987687, "Entwickler", "18.12.2010", 5633.72);
		mitarbeiter[43] = new Mitarbeiter("Lacy", "Hull", 74914724, "QS", "09.06.1977", 5729.66);
		mitarbeiter[44] = new Mitarbeiter("Nicky", "Cochran", 22762387, "Bereichsleiter", "02.04.1986", 6758.48);
		mitarbeiter[45] = new Mitarbeiter("Tamra", "Bridges", 25513463, "Entwickler", "07.08.2009", 4282.96);
		mitarbeiter[46] = new Mitarbeiter("Seymour", "Chaney", 91938660, "QS", "23.10.1995", 3353.00);
		mitarbeiter[47] = new Mitarbeiter("Arturo", "Norris", 58301216, "Azubi", "06.10.2012", 2784.72);
		mitarbeiter[48] = new Mitarbeiter("Bill", "Waller", 50489999, "Entwickler", "05.02.1975", 6782.32);
		mitarbeiter[49] = new Mitarbeiter("Dominique", "Zamora", 49573338, "Entwickler", "27.05.1995", 6157.29);
	}

	public void sucheMitarbeiter(int personalnummer)
	{
		// TODO
	}

	public void sucheMitarbeiter(String nachname)
	{
		// TODO
	}
}
